FROM python:3.10

RUN pip install --upgrade pip

WORKDIR /examplerizer

# Install the pinned requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# Install the wheel. Requirements are already installed
COPY dist/* .
RUN pip install *.whl
